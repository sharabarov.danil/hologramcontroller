# ARHologramController
**The plugin for creation of Virtual Object in your application.**

You can create Hologram (virtual object) in AR on devices or in the default position on the screen in Unity Editor. 
This plugin also allows to change position and scale current Hologram.


Stack of technologies:
1. New Input System
2. AR Foundation
