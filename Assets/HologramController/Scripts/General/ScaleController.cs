using UnityEngine;

namespace Hologram {

    /// <summary>
    /// Scale Controller
    /// </summary>
    public class ScaleController : AbstractScaleController {

        private Transform _hologram;
        private float _speed;
        private Vector2 _limits;

        public ScaleController(Transform hologram, float speed, Vector2 limits) {
            _hologram = hologram;
            _speed = speed;
            _limits = limits;
        }

        public override Vector3? Scale(float source) {
            float tmpScale = _hologram.localScale.y;
            tmpScale += source * _speed;
            tmpScale = Mathf.Clamp(tmpScale, _limits.x, _limits.y);
            return Vector3.one * tmpScale;
        }
    }
}
