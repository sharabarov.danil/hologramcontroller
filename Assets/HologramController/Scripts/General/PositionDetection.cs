
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Position Detection
    /// </summary>
    public class PositionDetection : AbstractPositionDetection {

        private Transform _hologram;
        private TouchControls _controls;
        private CancellationTokenSource _positionToken;
        private AbstractPositionController _abstractPositionController;

        public override void Init(Transform hologram, AbstractPositionController abstractPositionController) {
            _controls = new TouchControls();
            _hologram = hologram;
            _abstractPositionController = abstractPositionController;

            Enable();
            Subscribe();
        }

        private void Enable() {
            _controls.Enable();
        }

        private void Subscribe() {
            _controls.Touch.PrimaryTouchContact.started += _ => OnPositionStart();
            _controls.Touch.PrimaryTouchContact.canceled += _ => OnPositionEnd();
        }

        private void Disable() {
            _controls.Disable();
        }

        private void Unsubscribe() {
            _controls.Touch.PrimaryTouchContact.started -= _ => OnPositionStart();
            _controls.Touch.PrimaryTouchContact.canceled -= _ => OnPositionEnd();
        }

        private async void OnPositionStart() {
            _positionToken = new CancellationTokenSource();
            CancellationToken cancellationToken = _positionToken.Token;
            while (!cancellationToken.IsCancellationRequested) {
                Vector2 tmpDir = _controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>();
                float height = MeshSize.GetSize(_hologram).y;
                if (_abstractPositionController.Position(tmpDir, height) != null) {
                    _hologram.position = _abstractPositionController.Position(tmpDir, height).Value;
                }
                await Task.Yield();
            }
        }

        private void OnPositionEnd() {
            if (_positionToken != null) {
                _positionToken.Cancel();
                _positionToken = null;
            }
        }

        public override void Dispose() {
            Unsubscribe();
            Disable();
        }
    }
}
