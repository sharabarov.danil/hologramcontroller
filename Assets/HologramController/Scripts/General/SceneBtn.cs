using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Hologram {

    /// <summary>
    /// Scene Btn
    /// </summary>
    public class SceneBtn : MonoBehaviour, IPointerClickHandler {
        [SerializeField]
        private string _sceneName;
        public void OnPointerClick(PointerEventData eventData) {
            SceneManager.LoadScene(_sceneName);
        }
    }
}
