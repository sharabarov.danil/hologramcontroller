using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Hologram {

    /// <summary>
    /// Pinch Detection
    /// </summary>
    public class PinchDetection : AbstractScaleDetection {

        private Transform _hologram;
        private TouchControls _controls;
        private CancellationTokenSource _pinchToken;
        private AbstractScaleController _abstractScaleController;

        public override void Init(Transform hologram, AbstractScaleController abstractScaleController) {
            _controls = new TouchControls();
            _hologram = hologram;
            _abstractScaleController = abstractScaleController;
            Enable();
            Subscribe();
        }

        private void Enable() {
            _controls.Enable();
        }

        private void Subscribe() {
            _controls.Touch.SecondaryTouchContact.started += _ => OnPinchStart();
            _controls.Touch.SecondaryTouchContact.canceled += _ => OnPinchEnd();
        }

        private void Disable() {
            _controls.Disable();
        }

        private void Unsubscribe() {
            _controls.Touch.SecondaryTouchContact.started -= _ => OnPinchStart();
            _controls.Touch.SecondaryTouchContact.canceled -= _ => OnPinchEnd();
        }

        private async void OnPinchStart() {
            _pinchToken = new CancellationTokenSource();
            CancellationToken cancellationToken = _pinchToken.Token;
            float previousDistance = Vector2.Distance(_controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>(), _controls.Touch.SecondaryFingerPosition.ReadValue<Vector2>());
            float distance = previousDistance;
            while (!cancellationToken.IsCancellationRequested) {
                distance = Vector2.Distance(_controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>(), _controls.Touch.SecondaryFingerPosition.ReadValue<Vector2>());
                if (_abstractScaleController.Scale(distance - previousDistance) != null) {
                    _hologram.localScale = _abstractScaleController.Scale(distance - previousDistance).Value;
                }
                previousDistance = distance;
                await Task.Yield();
            }
        }

        private void OnPinchEnd() {
            if (_pinchToken != null) {
                _pinchToken.Cancel();
                _pinchToken = null;
            }
        }

        public override void Dispose() {
            Unsubscribe();
            Disable();
        }

    }
}
