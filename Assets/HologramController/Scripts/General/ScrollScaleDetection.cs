using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Scroll Scale Detection
    /// </summary>
    public class ScrollScaleDetection : AbstractScaleDetection {

        private Transform _hologram;
        private TouchControls _controls;
        private AbstractScaleController _abstractScaleController;

        public override void Init(Transform hologram, AbstractScaleController abstractScaleController) {
            _controls = new TouchControls();
            _hologram = hologram;
            _abstractScaleController = abstractScaleController;
            Enable();
            Subscribe();
        }


        private void Enable() {
            _controls.Enable();
        }

        private void Subscribe() {
            _controls.Touch.Scroll.started += _ => OnScrollStart();
        }

        private void Disable() {
            _controls.Disable();
        }

        private void Unsubscribe() {
            _controls.Touch.Scroll.started -= _ => OnScrollStart();
        }

        private void OnScrollStart() {
            float scrollValue = _controls.Touch.Scroll.ReadValue<Vector2>().y;
            if (_abstractScaleController.Scale(scrollValue) != null) {
                _hologram.localScale = _abstractScaleController.Scale(scrollValue).Value;
            }
        }

        public override void Dispose() {
            Unsubscribe();
            Disable();
        }


    }
}