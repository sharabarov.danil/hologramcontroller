using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Get Mesh Size
    /// </summary>
    public static class MeshSize {

        /// <summary>
        /// Get Size
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Vector3 GetSize(Transform obj) {
            MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
            Mesh mesh = null;
            if (meshFilter != null) {
                mesh = meshFilter.mesh;
            }
            if (mesh != null) {
                Vector3 objectSize = Vector3.Scale(obj.localScale, mesh.bounds.size);
                return objectSize;
            }
            return Vector3.zero;
        }
    }
}
