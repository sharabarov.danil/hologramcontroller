using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Hologram {

    /// <summary>
    /// AR hologram Creator
    /// </summary>
    public class ARHologramCreator : MonoBehaviour {

        [SerializeField]
        private Transform _hologramPrefab;
        [SerializeField]
        private ARRaycastManager _arRaycastManager;
        [SerializeField]
        private TrackableType _trackableType;

        private TouchControls _controls;
        private Transform _hologram;
        private AbstractPositionController _abstractPositionController;

        private void Awake() {
            _controls = new TouchControls();
            _abstractPositionController = new ARPositionController(_arRaycastManager, _trackableType);
        }

        private void OnEnable() {
            _controls.Enable();
        }

        private void OnDisable() {
            _controls.Disable();
        }

        private void Start() {
            _controls.Touch.PrimaryTouchContact.started += _ => OnCreationStart();
        }

        private void OnDestroy() {
            _controls.Touch.PrimaryTouchContact.started -= _ => OnCreationStart();
        }

        private void OnCreationStart() {
            Vector2 source = _controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>();
            if (_abstractPositionController.Position(source) != null) {
                _hologram = Instantiate(_hologramPrefab);
                float height = MeshSize.GetSize(_hologram).y;
                _hologram.transform.position = _abstractPositionController.Position(source, height).Value;
            }
            Destroy(this.gameObject);
        }
    }
}