using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Hologram {

    /// <summary>
    /// AR Position Controller
    /// </summary>
    public class ARPositionController : AbstractPositionController {

        private ARRaycastManager _arRaycastManager;
        private TrackableType _trackableType;
        private List<ARRaycastHit> _hits = new List<ARRaycastHit>();

        public ARPositionController(ARRaycastManager arRaycastManager, TrackableType trackableType) {
            _arRaycastManager = arRaycastManager;
            _trackableType = trackableType;
        }

        public override Vector3? Position(Vector2 source, float height) {
            Ray ray = Camera.main.ScreenPointToRay(source);
            if (_arRaycastManager.Raycast(ray, _hits, _trackableType)) {
                var hitPose = _hits[0].pose.position;
                return hitPose + Vector3.up * height / 2f;
            }
            return null;
        }
    }
}
