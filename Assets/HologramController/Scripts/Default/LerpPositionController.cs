using UnityEngine;
namespace Hologram {
    /// <summary>
    /// Lerp Position Controller
    /// </summary>
    public class LerpPositionController : AbstractPositionController {

        private Transform _hologram;
        private LayerMask _layerMask;
        private int _maxDistance;
        private float _speed;

        public LerpPositionController(Transform hologram, LayerMask layerMask, int maxDistance, float speed) {
            _hologram = hologram;
            _layerMask = layerMask;
            _maxDistance = maxDistance;
            _speed = speed;
        }

        public override Vector3? Position(Vector2 source, float height) {
            Ray ray = Camera.main.ScreenPointToRay(source);
            if (Physics.Raycast(ray, out RaycastHit raycastHit, _maxDistance, _layerMask)) {
                return Vector3.Lerp(_hologram.position, raycastHit.point + Vector3.up * height / 2f, Time.deltaTime * _speed);
            }
            return null;
        }
    }
}
