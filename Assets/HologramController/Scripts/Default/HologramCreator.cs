using UnityEngine;
namespace Hologram {
    /// <summary>
    /// Hologram Creator
    /// </summary>
    public class HologramCreator : MonoBehaviour {

        [SerializeField]
        private Transform _hologramPrefab;
        [SerializeField]
        private LayerMask _layerMask;
        [SerializeField]
        private int _maxDistance = 100;

        private TouchControls _controls;
        private Transform _hologram;
        private AbstractPositionController _abstractPositionController;

        private void Awake() {
            _controls = new TouchControls();
            _abstractPositionController = new PositionController(_layerMask, _maxDistance);
        }

        private void OnEnable() {
            _controls.Enable();
        }

        private void OnDisable() {
            _controls.Disable();
        }

        private void Start() {
            _controls.Touch.PrimaryTouchContact.started += _ => OnCreationStart();
        }

        private void OnDestroy() {
            _controls.Touch.PrimaryTouchContact.started -= _ => OnCreationStart();
        }

        private void OnCreationStart() {
            Vector2 source = _controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>();
            if (_abstractPositionController.Position(source) != null) {
                _hologram = Instantiate(_hologramPrefab);
                float height = MeshSize.GetSize(_hologram).y;
                _hologram.transform.position = _abstractPositionController.Position(source, height).Value;
            }
            Destroy(this.gameObject);
        }
    }
}
