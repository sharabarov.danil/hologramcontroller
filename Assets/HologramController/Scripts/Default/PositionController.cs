using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Position Controller
    /// </summary>
    public class PositionController : AbstractPositionController {

        private LayerMask _layerMask;
        private int _maxDistance;

        public PositionController(LayerMask layerMask, int maxDistance) {
            _layerMask = layerMask;
            _maxDistance = maxDistance;
        }

        public override Vector3? Position(Vector2 source, float height) {
            Ray ray = Camera.main.ScreenPointToRay(source);
            if (Physics.Raycast(ray, out RaycastHit raycastHit, _maxDistance, _layerMask)) {
                return raycastHit.point + Vector3.up * height / 2f;
            }
            return null;
        }
    }
}
