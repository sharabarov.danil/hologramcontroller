using UnityEngine;
namespace Hologram {
    /// <summary>
    /// Hologram Constructor
    /// </summary>
    public class HologramConstructor : MonoBehaviour {

        [Header("Position")]
        [Space]
        [SerializeField]
        private float _positionSpeed = 20f;
        [SerializeField]
        private LayerMask _layerMask;
        [SerializeField]
        private int _maxDistance = 100;

        [Header("Scale")]
        [Space]
        [SerializeField]
        private float _scrollSpeed = 0.001f;
        [SerializeField]
        private float _pinchSpeed = 0.05f;
        [SerializeField]
        private Vector2 _scaleLimit = new Vector2(0.5f, 2f);

        private Transform _hologram;

        private AbstractPositionDetection _positionDetection = new PositionDetection();
        private AbstractScaleDetection _scrollScaleDetection = new ScrollScaleDetection();
        private AbstractScaleDetection _pinchDetection = new PinchDetection();

        private AbstractPositionController _positionController;
        private AbstractScaleController _scrollScaleController;
        private AbstractScaleController _pinchController;

        private void Start() {

            _hologram = this.gameObject.transform;

            _positionController = new LerpPositionController(_hologram, _layerMask, _maxDistance, _positionSpeed);
            _scrollScaleController = new ScaleController(_hologram, _scrollSpeed, _scaleLimit);
            _pinchController = new ScaleController(_hologram, _pinchSpeed, _scaleLimit);

            _positionDetection.Init(_hologram, _positionController);
            _scrollScaleDetection.Init(_hologram, _scrollScaleController);
            _pinchDetection.Init(_hologram, _pinchController);
        }

        private void OnDestroy() {
            _positionDetection.Dispose();
            _scrollScaleDetection.Dispose();
            _pinchDetection.Dispose();
        }
    }
}
