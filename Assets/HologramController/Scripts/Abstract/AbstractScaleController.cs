using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Abstract scale changer
    /// </summary>
    public abstract class AbstractScaleController {
        /// <summary>
        /// Abstract Scale for hologram
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract Vector3? Scale(float source);
    }
}
