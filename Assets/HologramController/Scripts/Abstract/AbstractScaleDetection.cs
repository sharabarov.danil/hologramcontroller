using UnityEngine;

namespace Hologram {
    /// <summary>
    /// Abstract Detection
    /// </summary>
    public abstract class AbstractScaleDetection {

        /// <summary>
        /// Init Hologram
        /// </summary>
        /// <param name="hologram"></param>
        /// <param name="abstractScaleController"></param>
        public abstract void Init(Transform hologram, AbstractScaleController abstractScaleController);

        /// <summary>
        /// Dispose Hologram Detection
        /// </summary>
        public abstract void Dispose();
    }
}