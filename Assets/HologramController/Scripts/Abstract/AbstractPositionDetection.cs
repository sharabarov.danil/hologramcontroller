using UnityEngine;

namespace Hologram {

    /// <summary>
    /// Abstract Detection
    /// </summary>
    public abstract class AbstractPositionDetection {

        /// <summary>
        /// Init Hologram
        /// </summary>
        /// <param name="hologram"></param>
        /// <param name="abstractPositionController"></param>
        public abstract void Init(Transform hologram, AbstractPositionController abstractPositionController);

        /// <summary>
        /// Dispose Hologram Detection
        /// </summary>
        public abstract void Dispose();
    }
}
