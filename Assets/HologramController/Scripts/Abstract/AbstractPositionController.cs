using UnityEngine;

namespace Hologram {

    /// <summary>
    /// Abtract Position Controller
    /// </summary>
    public abstract class AbstractPositionController {
        /// <summary>
        /// Abstract Position for hologram
        /// </summary>
        /// <param name="source"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public abstract Vector3? Position(Vector2 source, float height = 0);
    }
}
